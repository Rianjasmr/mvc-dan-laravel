<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>FORM HTML</title>
</head>

<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>

    <form action="/masuk" method="post">
        @csrf
        <label for="first">First name:</label><br><br>
        <input type="text" name="first_name" id="first"><br><br>

        <label for="last">Last name:</label><br><br>
        <input type="text" name="last_name" id="last"><br><br>

        <label for="gender">Gender:</label><br><br>
        <input type="radio" name="gender"> Male <br>
        <input type="radio" name="gender"> Female <br>
        <input type="radio" name="gender"> Other <br><br>

        <label for="negara">Nationality:</label><br><br>
        <select name="negara">
            <option value="NGR">Indonesia</option>
            <option value="NGR">Malaysia</option>
            <option value="NGR">China</option>
        </select><br><br>

        <label for="skill">Language Spoken:</label><br><br>
        <input type="checkbox" id="skill"> Bahasa Indonesia <br>
        <input type="checkbox" id="skill"> English <br>
        <input type="checkbox" id="skill"> Other <br><br>

        <label for="bio">Bio:</label><br><br>
        <textarea name="bio" id="" cols="30" rows="10"></textarea><br>

        <input type="submit" value="SIGN UP">

    </form>





</body>

</html>